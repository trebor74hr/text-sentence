# coding=utf-8
import sentence
from   sentence import (Tokenizer, tokenize, ItemList, Abbr, 
                        Name, TokenizerParams, Token, test)

__all__ = [ "Tokenizer", "tokenize", "ItemList", "Abbr", 
            "Name", "TokenizerParams", "Token", "test"]

if __name__ == "__main__":
    test()


